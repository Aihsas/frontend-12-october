import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup,FormBuilder } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DeleteblogService } from '../../service/deleteblog.service';
@Component({
  selector: 'app-listblog',
  templateUrl: './listblog.component.html',
  styleUrls: ['./listblog.component.scss']
})
export class ListblogComponent implements OnInit {
  addblog: FormGroup;
  submitted = false;
  listArr: any = [];
  constructor(private deleteblogs:DeleteblogService,public formBuilder:FormBuilder, public httpClient: HttpClient) { }

  ngOnInit() {
    this.addblog = new FormGroup({
      "name": new FormControl(''),
      "description": new FormControl(''),
  })
  this.httpClient.get("http://localhost:3000/bloglisting").subscribe(data=> {
  console.log("response: ", data)
  this.listArr = data;
})
  
  }
  
  
  deleteblog(id){
    this.deleteblogs.deleteblog(id).subscribe(res => {
      console.log('Deleted');    
    location.reload()
    });
  }
}
  
  
  
  

