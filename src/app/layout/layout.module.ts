import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
//import { ListitemComponent } from './listitem/listitem.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListitemComponent } from './listitem/listitem.component';
import { ListuserComponent } from './listuser/listuser.component';
import { ListitemeditComponent } from './listitemedit/listitemedit.component';
import { AddblogComponent } from './addblog/addblog.component';
import { ListblogComponent } from './listblog/listblog.component';
import { ViewbloggComponent } from './viewblogg/viewblogg.component';
import {PaginatorModule} from 'primeng/paginator';
import { AuthService } from '../service/auth.service';
import { AuthGuard } from '../shared';
import {TableModule} from 'primeng/table';
import {ButtonModule} from 'primeng/button';
import {ToastModule} from 'primeng/toast';

@NgModule({
    imports: [FormsModule,ReactiveFormsModule,
        CommonModule,
        LayoutRoutingModule,
        TranslateModule,PaginatorModule,
        NgbDropdownModule.forRoot(),
        TableModule,
        ButtonModule,
        ToastModule
    ],
    declarations: [LayoutComponent, SidebarComponent, HeaderComponent, ListitemComponent, ListuserComponent, ListitemeditComponent, AddblogComponent, ListblogComponent,  ViewbloggComponent]
    ,providers: [AuthGuard,AuthService],
})
export class LayoutModule {}
