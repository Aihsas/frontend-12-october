import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';
import { AdditemService } from '../../service/additem.service';
import { AddblogService } from '../../service/addblog.service';
@Component({
  selector: 'app-addblog',
  templateUrl: './addblog.component.html',
  styleUrls: ['./addblog.component.scss']
})
export class AddblogComponent implements OnInit {
  addblog: FormGroup;
  submitted = false;
  listArr: any = [];
  

 
  constructor( private addblogs:AddblogService,private fb:FormBuilder) { }

  ngOnInit() {
    this.addblog = this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      avatar: null
  })

}
get f() { return this.addblog.controls; }

addBlogs() {
  const formModel = this.prepareSave();
  console.log("Registration form :", this.addblog.value)
  this.submitted = true;


  this.addblogs
      .addBlogs(formModel)
      .subscribe(data => {

      })
  // console.log(this.registerForm.value)
}
onFileChange(event) {
  if(event.target.files.length > 0) {
    let file = event.target.files[0];
    this.addblog.get('avatar').setValue(file);
  }
}
private prepareSave(): any {
  let input = new FormData();
  // This can be done a lot prettier; for example automatically assigning values by looping through `this.form.controls`, but we'll keep it as simple as possible here
  input.append('name', this.addblog.get('name').value);
  input.append('description', this.addblog.get('description').value);
  input.append('file', this.addblog.get('avatar').value);

  return input;
}


}


