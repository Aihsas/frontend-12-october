import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewbloggComponent } from './viewblogg.component';

describe('ViewbloggComponent', () => {
  let component: ViewbloggComponent;
  let fixture: ComponentFixture<ViewbloggComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewbloggComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewbloggComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
