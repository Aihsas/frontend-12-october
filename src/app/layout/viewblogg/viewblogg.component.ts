import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup,FormBuilder } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-viewblogg',
  templateUrl: './viewblogg.component.html',
  styleUrls: ['./viewblogg.component.scss']
})
export class ViewbloggComponent implements OnInit {
  addblog: FormGroup;
  submitted = false;
  listArr: any = [];
  constructor(public formBuilder:FormBuilder, public httpClient: HttpClient) { }

  ngOnInit() {
    this.addblog = new FormGroup({
      "name": new FormControl(''),
      "description": new FormControl(''),
  })
  this.httpClient.get("http://localhost:3000/bloglisting").subscribe(data=> {
  console.log("response: ", data)
  this.listArr = data;
})
  
  }
}
