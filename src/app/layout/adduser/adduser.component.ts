import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators} from '@angular/forms';
import { AdduserService } from '../../service/adduser.service';
import { SignupService } from '../../service/signup.service';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.scss']
})
export class AdduserComponent implements OnInit {
  submitted = false;
  listArr: any = [];
  adduser: FormGroup;
  constructor(private signupservices:SignupService,private formBuilder: FormBuilder) {}
    
  ngOnInit() {
    this.adduser = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      role: ['', Validators.required],
      
  });
  

}
get f() { return this.adduser.controls; }

onSubmit(){
  console.log("Registration form :", this.adduser.value)
  this.submitted = true;

  // stop here if form is invalid

  this.signupservices
      .onSubmit(this.adduser.value)
      .subscribe(data => {

      })
  // console.log(this.registerForm.value)
}
}









