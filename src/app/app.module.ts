import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { UserdashboardComponent } from './userdashboard/userdashboard.component';
import { AddblogsComponent } from './addblogs/addblogs.component';
// import { SignupComponent } from './signup/signup.component';
import { ViewblogComponent } from './viewblog/viewblog.component';
import { ViewproductComponent } from './viewproduct/viewproduct.component';
import {PaginatorModule} from 'primeng/paginator';
import {CardModule} from 'primeng/card';
import { AuthService } from './service/auth.service';
import { SignuppComponent } from './signupp/signupp.component';
import { ViewmoreComponent } from './viewmore/viewmore.component';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import {RatingModule} from 'primeng/rating';

export const createTranslateLoader = (http: HttpClient) => {

    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,RatingModule,
         HttpClientModule,ReactiveFormsModule,FormsModule,PaginatorModule,CardModule,ScrollPanelModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AppRoutingModule
    ],
    declarations: [AppComponent, HomeComponent, UserdashboardComponent, AddblogsComponent, ViewblogComponent, ViewproductComponent, SignuppComponent, ViewmoreComponent],
    providers: [AuthGuard,AuthService],
    bootstrap: [AppComponent]
})
export class AppModule {}
