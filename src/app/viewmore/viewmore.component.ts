import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup,FormBuilder } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-viewmore',
  templateUrl: './viewmore.component.html',
  styleUrls: ['./viewmore.component.scss']
})
export class ViewmoreComponent implements OnInit {
  isLoggedIn : boolean = false;

  addblog: FormGroup;
  submitted = false;
  listArr: any = [];
  constructor(public formBuilder:FormBuilder, public httpClient: HttpClient) { 
  let authToken = localStorage.getItem("Angular5Demo-Token");
  console.log("auth : ", authToken)
  if(authToken) {
    this.isLoggedIn = true;
}
  }
  ngOnInit() {
    this.addblog = new FormGroup({
      "name": new FormControl(''),
      "description": new FormControl(''),
  })
  this.httpClient.get("http://localhost:3000/bloglisting").subscribe(data=> {
  console.log("response: ", data)
  this.listArr = data;
})

}
}


