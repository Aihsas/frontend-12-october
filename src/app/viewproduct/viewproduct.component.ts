import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup,FormBuilder } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-viewproduct',
  templateUrl: './viewproduct.component.html',
  styleUrls: ['./viewproduct.component.scss']
})
export class ViewproductComponent implements OnInit {
  isLoggedIn : boolean = false;

  constructor(public httpClient: HttpClient,private router: Router) { 
    let authToken = localStorage.getItem("Angular5Demo-Token");
    console.log("auth : ", authToken)
    if(authToken) {
      this.isLoggedIn = true;
    }
  }
  addproduct: FormGroup;
  submitted = false;
  listArr: any = [];
  ngOnInit() {
    this.addproduct = new FormGroup({
      "pname": new FormControl(''),
      "pdescription": new FormControl(''),
      "pcost": new FormControl(''),
      "status": new FormControl(''),
      
  })
  this.httpClient.get("http://localhost:3000/listing").subscribe(data=> {
  console.log("response: ", data)
  this.listArr = data;

})
}
logout(){
  localStorage.removeItem("Angular5Demo-Token")
  this.router.navigate(["home"]);
  this.isLoggedIn = false
}
}
