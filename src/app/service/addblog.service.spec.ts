import { TestBed, inject } from '@angular/core/testing';

import { AddblogService } from './addblog.service';

describe('AddblogService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddblogService]
    });
  });

  it('should be created', inject([AddblogService], (service: AddblogService) => {
    expect(service).toBeTruthy();
  }));
});
