import { TestBed, inject } from '@angular/core/testing';

import { DeleteblogService } from './deleteblog.service';

describe('DeleteblogService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeleteblogService]
    });
  });

  it('should be created', inject([DeleteblogService], (service: DeleteblogService) => {
    expect(service).toBeTruthy();
  }));
});
