import { TestBed, inject } from '@angular/core/testing';

import { DeletelistitemService } from './deletelistitem.service';

describe('DeletelistitemService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeletelistitemService]
    });
  });

  it('should be created', inject([DeletelistitemService], (service: DeletelistitemService) => {
    expect(service).toBeTruthy();
  }));
});
