import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  //BaseUrl : any = "http:localhost:3000/";
  constructor(private http:HttpClient) { }
  onLogin(data):Observable<any>{
    console.log("login form data : ", data)
  return this.http.post("http://localhost:3000/adminLogin",data);

}

}

