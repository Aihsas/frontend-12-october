import { TestBed, inject } from '@angular/core/testing';

import { EdititemService } from './edititem.service';

describe('EdititemService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EdititemService]
    });
  });

  it('should be created', inject([EdititemService], (service: EdititemService) => {
    expect(service).toBeTruthy();
  }));
});
