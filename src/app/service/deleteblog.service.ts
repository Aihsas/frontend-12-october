import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DeleteblogService {
  constructor(private http:HttpClient) { }
  deleteblog(id):Observable<any>{
    console.log("login form data : ", id)
    return this.http.delete('http://localhost:3000/deletebloglist?id='+id)
  
   
}

}

