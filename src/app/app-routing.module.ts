import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { HomeComponent } from './home/home.component';
import { UserdashboardComponent } from './userdashboard/userdashboard.component';
import { ViewblogComponent } from './viewblog/viewblog.component';
import { ViewproductComponent } from './viewproduct/viewproduct.component';
import { SignuppComponent } from './signupp/signupp.component';
import { ViewmoreComponent } from './viewmore/viewmore.component';
//import { AddblogsComponent } from './addblogs/addblogs.component';
//import { DashboardComponent } from './layout/dashboard/dashboard.component';
//
const routes: Routes = [
    { path: '', component:HomeComponent },
    { path: 'login', loadChildren: './login/login.module#LoginModule' },
    // { path: 'signup', loadChildren: './signup/signup.module#SignupModule' },
    { path:'layout', loadChildren:'./layout/layout.module#LayoutModule'},
    { path:'dashboard', loadChildren:'./layout/layout.module#LayoutModule',},
    { path: 'error', loadChildren: './server-error/server-error.module#ServerErrorModule' },
    { path: 'access-denied', loadChildren: './access-denied/access-denied.module#AccessDeniedModule' },
    { path: 'tables', loadChildren: './layout/tables/tables.module#TablesModule',canActivate: [AuthGuard]},
    { path: 'charts', loadChildren: './layout/charts/charts.module#ChartsModule',canActivate: [AuthGuard]},
    { path: 'forms', loadChildren: './layout/charts/charts.module#ChartsModule',canActivate: [AuthGuard]}, 
    {
        path:'userdashboard', component:UserdashboardComponent
    },
    {
        path:'viewblog', component:ViewblogComponent
    },
    {
        path:'viewproduct', component:ViewproductComponent
    },
    {
        path:'home', component:HomeComponent
    },
    {
        path:'signupp', component:SignuppComponent
    },
    {
        path:'viewmore', component:ViewmoreComponent
    }
      
      
      
      




    // { path: 'not-found', loadChildren: './not-found/not-found.module#NotFoundModule' },
    // { path: '**', redirectTo: 'not-found' },
  
   
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
